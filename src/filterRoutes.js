// Hide banner
var heroCta = document.getElementsByClassName('herocta')[0];
var heroPrice = document.getElementsByClassName('heroprice-component')[0];

if (heroCta !== undefined) {
    heroCta.style.display = 'none';
}

if (heroPrice !== undefined) {
    heroPrice.style.display = 'none';
}

var countries = document.getElementsByClassName('country');

for(var country of countries) {
	if (country.textContent === 'Italia'){
		var father = country.parentElement.parentElement.parentElement;
		father.style.display = 'none';
	}
}

var routes = document.getElementsByTagName('sales-route');

for(var route of routes) {
	if(route.title !== 'Bologna') {
		route.style.display = 'none';
	}
}
